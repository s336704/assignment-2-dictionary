%define pointer 0

%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
            dq pointer
            db %1, 0 
            %define pointer %2
        %else
            %error "Second argument must be a label type"
        %endif
    %else
        %error "First argument must be a string type"
    %endif
%endmacro
