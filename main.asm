section .rodata
not_found_message: db "Not found", 0
too_long_message: db "Too long", 0

section .bss
buffer: resb 256 

section .data
%include "words.inc"

section .text
%include "lib.inc"
global _start

_start:
    mov rdi, buffer
    mov rsi, 256
    call read_word
    cmp rax, 0
    je .too_long

    mov rdi, rax
    mov rsi, pointer
    call find_word
    cmp rax, 0
    je .not_found

    .success:
        mov rdi, rax
        call print_string
        jmp .end
    .too_long:
        mov rdi, too_long_message
        call print_string
        jmp .end
    .not_found:
        mov rdi, not_found_message
        call print_error
    .end:
        call print_newline
        call exit
