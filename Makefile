ASM=nasm
ASMFLAGS=-f elf64
LD=ld

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean
clean:
	rm *.o

program: dict.o lib.o main.o
	$(LD) -o $@ $^
