global find_word

%include "lib.inc"

; rax = pointer
; 1) next pointer (8 bytes)
; 2) key (null-terminated string)
; 3) value (null-terminated string)

; rdi - string pointer
; rsi - dict pointer
find_word:
    mov rcx, rsi
    .loop:
        mov rsi, rcx
        add rsi, 8
        push rcx
        push rdi
        call string_equals
        pop rdi
        pop rcx
        cmp rax, 1
        je .found
        cmp qword [rcx], 0
        je .not_found
        mov rcx, [rcx]
        jmp .loop
    .found:
        ; rcx - start pointer
        ; rcx + 8 - key pointer
        ; rcx + 8 + length - value pointer
        add rcx, 8
        mov rdi, rcx
        inc rcx
        push rcx
        call string_length
        pop rcx
        add rax, rcx
        jmp .end
    .not_found:
        mov rax, 0
    .end:
        ret
